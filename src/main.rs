extern crate chrono;
use chrono::Local;
use std::thread ;
use std::time::Duration;
use std::fs::File;
use std::io::Write;
use std::env;
use std::fs;
use std::io;

fn main() {
    let debug = false;
    let output_dir = "${HOME}/.rumore/logs";

    let about_name = "rumore";
    let about_version = "1.0.0";
    let about_modified = "2019-04-23";
    let rundts = Local::now().format("%Y-%m-%d_%H%M%S");
    println!("{} - v.{} ({})", about_name, about_version, about_modified);
    let output_to = shellexpand::env(output_dir).unwrap();
    fs::create_dir_all(output_to.to_string())
        .expect("Unable to create output_dir");
    let log_base = format!("{}/{}_{}", output_to, rundts, about_name);

    // get loop_amount from envvar, else default to '300'.
    let loop_amount = env::var("LOOP_AMOUNT");
    let loop_amount = loop_amount
        .as_ref()
        .map(String::as_str)
        .unwrap_or("300");
    let loops: u64 = loop_amount.to_string().parse::<u64>().unwrap();
    // get pause_amount from envvar, else default to '2'.
    let pause_amount = env::var("PAUSE_AMOUNT");
    let pause_amount = pause_amount
        .as_ref()
        .map(String::as_str)
        .unwrap_or("2");
    let pause: u64 = pause_amount.to_string().parse::<u64>().unwrap();

    let mut cpu_sum: f32 = 0.0;
    let mut load_sum_one: f32 = 0.0;
    let mut load_sum_five: f32 = 0.0;
    let mut load_sum_fifteen: f32 = 0.0;
    let mut memory_sum: f64 = 0.0;

    let pause_time = Duration::new(pause, 0);
    println!("- rundts:   {}", rundts);
    println!("- run reqs: {}s (loops={}, pause={})", (loops * pause), loops, pause);
    print!("- processing..");
    io::stdout().flush().unwrap();

    let mut all_cpu = vec![];
    let mut all_load_one = vec![];
    let mut all_load_five = vec![];
    let mut all_load_fifteen = vec![];
    let mut all_memory = vec![];

    for x in 0..loops {
        if debug {
            println!("- iteration: {x}", x=x);
        }
        let cpu = probes::cpu::read().unwrap();
        if debug {
            println!("  - cpu: {} (%user)", cpu.stat.in_percentages().user);
            println!("  - cpu: {}/{}/{}/{} (%user/%system/%nice/%idle)", cpu.stat.in_percentages().user, cpu.stat.in_percentages().system, cpu.stat.in_percentages().nice, cpu.stat.in_percentages().idle);
            println!("  - cpu: {} (%user)", cpu.stat.total);
        }
        all_cpu.push(cpu.stat.in_percentages().user);
        cpu_sum += cpu.stat.in_percentages().user;

        let load = probes::load::read().unwrap();
        if debug {
            println!("  - load: {}/{}/{} (one/five/fifteen)", load.one, load.five, load.fifteen);
        }

        all_load_one.push(load.one);
        load_sum_one += load.one;
        all_load_five.push(load.five);
        load_sum_five += load.five;
        all_load_fifteen.push(load.fifteen);
        load_sum_fifteen += load.fifteen;

        let memory = probes::memory::read().unwrap();
        let mf: f64 = memory.free() as f64;
        let mt: f64 = memory.total() as f64;
        let mpf: f64 = (mf / mt) * 100 as f64;
        memory_sum += mpf;
        if debug {
            println!("  - memory: {} (%free)", mpf);
        }
        all_memory.push(mpf as f32);
        thread::sleep(pause_time);
        print!("..");
        io::stdout().flush().unwrap();
    }

    println!("");
    println!("------------------------------");
    println!("RESULT (averages shown)");
    println!("- cpu:     {} (% used of user)", cpu_sum/loops as f32);
    println!("- load:    {} (1m shown; 5m={}, 15m={})", load_sum_one/loops as f32, load_sum_five/loops as f32, load_sum_fifteen/loops as f32);
    println!("- memory:  {} (% free)", memory_sum/loops as f64);
    println!("------------------------------");

    println!("- persisting results...");

    // write cpu metrics.
    println!("  - writing cpu metrics");
    let cpu_file_name = format!("{}_cpu.csv", log_base);
    let mut cpu_file = File::create(cpu_file_name).expect("Unable to create cpu file!");
    cpu_file.write_all(b"user_utilization_percent\n").expect("Unable to write data");
    for i in all_cpu {
        let line = format!("{}\n",i);
        cpu_file.write_all(line.as_bytes()).expect("Unable to write data");
    }

    // write load metrics.
    println!("  - writing load metrics");
    let load_file_name = format!("{}_load.csv", log_base);
    let mut load_file = File::create(load_file_name).expect("Unable to create load file!");
    load_file.write_all(b"one_minute,five_minute,fifteen_minute\n").expect("Unable to write data");
    for (i, item) in all_load_one.iter().enumerate() {
        let line = format!("{},{},{}\n",item, all_load_five[i], all_load_fifteen[i]);
        load_file.write_all(line.as_bytes()).expect("Unable to write data");
    }

    // write memory metrics.
    println!("  - writing memory metrics");
    let memory_file_name = format!("{}_memory.csv", log_base);
    let mut memory_file = File::create(memory_file_name).expect("Unable to create memory file!");
    memory_file.write_all(b"free_percent\n").expect("Unable to write data");
    for i in all_memory {
        let line = format!("{}\n",i);
        memory_file.write_all(line.as_bytes()).expect("Unable to write data");
    }

    println!("- metrics persisted to: {}_*", log_base);
    println!("");   //this is needed to get the preceeding line to print.

}
