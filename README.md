# README

Rust Monitor Resources (rumore) is a lightweight, simple system resource monitor which, upon execution, runs in a loop, polling system resources at a given interval. Each poll result is stored and used to calculate the final result which consists of the average for each resource monitored. All resource metrics collected are wrote to a set of timestamped log files.

Monitored resources:
- cpu: cpu percent used (user)
- load: system load average
- memory: virtual memory used

This application is intended to be ran on the server which is being load tested to gather metrics of the server as load is underway. As such, this application was designed to consume as few resources (itself) as possible so as to not skew resource utilization by the load test.

Resource utilization of this application itself may vary based on the enviornment/architecture it is ran o. The following table shows resource utilization of this application along with other \*more variants:


| Application                              | Binary Size | VIRT   | RES    | SHR   | CPU% |
| ---------------------------------------- | :---------: | :----: | :----: | :---: | :--: |
| [pymore](https://gitlab.com/drad/pymore) | n/a         | 19,992 | 11,628 | 6,028 | 0.0  |
| [rumore](https://gitlab.com/drad/rumore) | 1.8M        | 2,928  | 888    | 768   | 0.0  |


## Build
- via cargo: `cargo build`


## Create Release
- via cargo: `cargo build --release`


## Run Requirements
- should be nothing required; however I have found some exceptions
  - CentOS: glibc is 2.17, rumore requires 2.18 - [more info here](https://github.com/denoland/deno/issues/1658)

## Run
- normal: `rumore`
  - via cargo: `cargo run`
- with custom loop amount and pause amount: `LOOP_AMOUNT=10 PAUSE_AMOUNT=3 ./rumore`
- the typical 10m run: `LOOP_AMOUNT=300 PAUSE_AMOUNT=2 ./rumore`


## Example Output
Example output running with 30 loops, 2 second pause (60s run):
```
rumore - v.1.0.0 (2019-04-23)
- rundts:   2019-04-24_120638
- run reqs: 60s (loops=30, pause=2)
- processing..............................................................
------------------------------
RESULT (averages shown)
- cpu:     6.9310117 (% used of user)
- load:    0.38000005 (1m shown; 5m=0.18600002, 15m=0.10966663)
- memory:  61.825306627897554 (% free)
------------------------------
- persisting results...
  - writing cpu metrics
  - writing load metrics
  - writing memory metrics
- metrics persisted to: /home/drad/.rumore/logs/2019-04-24_120638_rumore_*
```